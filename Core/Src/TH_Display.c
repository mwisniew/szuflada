#include "TH_Display.h"

char segment[10] = {0xFC, 0x60, 0xDA, 0xF2, 0x66, 0xB6, 0xBE, 0xE0, 0xFE, 0xF6};

void display_TH_init(I2C_HandleTypeDef* i2c){
  int i;
  uint8_t buffer[8];

  buffer[0] = THDISP_CONFIG_PORT;
  buffer[1] = THDISP_PINMODE_ALL_OUT;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);

  HAL_Delay(10);

  buffer[0] = THDISP_OUTPUT_PORT;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 1,  HAL_MAX_DELAY);

  // shift 24 zeros
  for(i = 0; i < 24; i++){
	  buffer[1] = 0x00;
	  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
	  HAL_Delay(25);

	  buffer[1] = THDISP_SHIFT;
	  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
	  HAL_Delay(25);

  }

  buffer[1] = THDISP_LATCH;
  buffer[2] = 0x00;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 3,  HAL_MAX_DELAY);

}

void display_TH(I2C_HandleTypeDef* i2c, float temp, float rh){

  int i;
  unsigned int _rh = (unsigned int) rh;
  int temp_nums = (int) (temp * 10.0f);
  if(temp_nums < 0)
    temp_nums = -temp_nums;
  char tosend = 0x00;
  uint8_t buffer[8];


  buffer[0] = THDISP_OUTPUT_PORT;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 1,  HAL_MAX_DELAY);

  for(i = 0; i < 8; i++){
    if((segment[(temp_nums / 100) % 10] & (1 << i) ) != 0 ){
      tosend |= THDISP_TFLAG_HIGH;
    }
    if((segment[(_rh / 10) % 10] & (1 << i) ) != 0){
      tosend |= THDISP_HFLAG_HIGH;
    }

    tosend |= THDISP_SHIFT;
    buffer[1] = tosend;
    HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
    HAL_Delay(25);

    tosend &= ~THDISP_SHIFT;
    buffer[1] = tosend;
	HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
	HAL_Delay(25);


    tosend = 0x00;
  }

  buffer[1] = THDISP_SHIFT | THDISP_LATCH;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);


  for(i = 0; i < 10; i++)
    segment[i]++;

  for(i = 0; i < 8; i++){
    if((segment[(temp_nums / 10) % 10] & (1 << i) ) != 0 ){
      tosend |= THDISP_TFLAG_HIGH;
    }
    if((segment[ _rh  % 10] & (1 << i) ) != 0){
      tosend |= THDISP_HFLAG_HIGH;
    }

    tosend |= THDISP_SHIFT;

    buffer[1] = tosend;
	HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
	HAL_Delay(25);

    tosend &= ~THDISP_SHIFT;
    buffer[1] = tosend;
	HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
	HAL_Delay(25);


    tosend = 0x00;
  }

  buffer[1] = THDISP_SHIFT_RH;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
  HAL_Delay(25);
  buffer[1] = THDISP_LATCH;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);

  for(i = 0; i < 10; i++)
    segment[i]--;

  for(i = 0; i < 8; i++){
    if((segment[(temp_nums) % 10] & (1 << i) ) != 0 ){
      tosend |= THDISP_TFLAG_HIGH;
    }

    tosend |= THDISP_SHIFT_TEMP;

    buffer[1] = tosend;
	HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
	HAL_Delay(25);

    tosend &= ~THDISP_SHIFT_TEMP;
    buffer[1] = tosend;
	HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);
	HAL_Delay(25);


    tosend = 0x00;
  }

  buffer[1] = THDISP_LATCH_TEMP;
  HAL_I2C_Master_Transmit(i2c, THDISP_ADDR >> 1, buffer, 2,  HAL_MAX_DELAY);


}
