#include "HC_SR04.h"

uint32_t HC_SR04_read_distance_uart(UART_HandleTypeDef* uart){
	uint8_t buffer[4];
	uint32_t distance = 0;
	buffer[3] = 0xA0;

	HAL_UART_Transmit(uart, (buffer + 3), 1, HAL_MAX_DELAY);
	HAL_Delay(150);
	//__HAL_UART_SEND_REQ(uart, UART_RXDATA_FLUSH_REQUEST);
	//HAL_Delay(30);


	HAL_UART_Receive(uart, buffer, 3, HAL_MAX_DELAY);

	distance = ((uint32_t)buffer[0] * 65536U + (uint32_t)buffer[1] * 256U + (uint32_t)buffer[2]) / 10000U;

	if(distance < 900)
		return distance;
	else return 0;

}

float HC_SR04_read_distance(TIM_HandleTypeDef* incap, uint32_t in1, uint32_t in2){
	uint32_t start = HAL_TIM_ReadCapturedValue(incap, in2);
	uint32_t stop = HAL_TIM_ReadCapturedValue(incap, in1);
	float val_f = (stop-start) / 5.8f;
	return val_f;

}
