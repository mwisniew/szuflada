#ifndef TH_DISPLAY_H_
#define TH_DISPLAY_H_

#include "stm32l4xx_hal.h"

#define THDISP_ADDR 0x80
#define THDISP_CONFIG_PORT 0x03
#define THDISP_PINMODE_ALL_OUT 0x00
#define THDISP_OUTPUT_PORT 0x01

#define THDISP_SHIFT 0x12
#define THDISP_SHIFT_TEMP 0x02
#define THDISP_SHIFT_RH 0x10

#define THDISP_LATCH 0x24
#define THDISP_LATCH_TEMP 0x04

#define THDISP_TFLAG_HIGH 0x01
#define THDISP_HFLAG_HIGH 0x08

extern char segment[10]; //= {0xFC, 0x60, 0xDA, 0xF2, 0x66, 0xB6, 0xBE, 0xE0, 0xFE, 0xF6};

void display_TH_init(I2C_HandleTypeDef* i2c);
void display_TH(I2C_HandleTypeDef*i2c, float temp, float rh);

#endif /* TH_DISPLAY_H_ */
