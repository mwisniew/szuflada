#ifndef INC_DHT11_H_
#define INC_DHT11_H_

#include "stdint.h"

typedef struct humidity {
	int8_t integral;
	int8_t deciaml;
};

typedef struct temp {
	int8_t intergral;
	int8_t deciaml;
};

typedef struct DHT11_struct {
	struct humidity hum;
	struct temp t;
};

typedef struct DHT11_converted_data {
	double temp;
	double hum;
};

uint8_t DHT11_init (struct DHT11_struct* DHT11);
uint8_t DHT11_start_measurement(void);
uint8_t DHT11_check_if_measurement_done(void);
struct DHT11_converted_data DHT11_get_measurement(void);

#endif /* INC_DHT11_H_ */
