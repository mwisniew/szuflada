#ifndef INC_HC_SR04_H_
#define INC_HC_SR04_H_

#include "usart.h"
#include "tim.h"

uint32_t HC_SR04_read_distance_uart(UART_HandleTypeDef* uart);
float HC_SR04_read_distance(TIM_HandleTypeDef* incap, uint32_t in1, uint32_t in2);

#endif /* INC_HC_SR04_H_ */
