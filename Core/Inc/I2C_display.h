#ifndef INC_I2C_DISPLAY_H_
#define INC_I2C_DISPLAY_H_

#include "stdint.h"

typedef struct display_lines {
	uint8_t line_0[16];
	uint8_t line_1[16];
};


typedef struct ISC_display_struct {
	struct display_lines lines;
};

uint8_t I2C_init_display (struct ISC_display_struct* ISC_display);
uint8_t I2C_display (uint8_t* text, uint8_t line);
uint8_t I2C_clear_display (void);
uint8_t I2C_change_backlight_state (uint8_t if_on);
uint8_t I2C_set_cursor_position (uint8_t horizontal, uint8_t line);
uint8_t I2C_write_from_cursor (uint8_t* text);

#endif /* INC_I2C_DISPLAY_H_ */
